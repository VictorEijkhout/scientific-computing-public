% -*- latex -*-
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%% This text file is part of the source of 
%%%% `Introduction to High-Performance Scientific Computing'
%%%% by Victor Eijkhout, copyright 2012-2021
%%%%
%%%% This book is distributed under a Creative Commons Attribution 3.0
%%%% Unported (CC BY 3.0) license and made possible by funding from
%%%% The Saylor Foundation \url{http://www.saylor.org}.
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lstset{language=CMake}

\Level 0 {CMake as build system}

\indexterm{CMake} is a general build system that uses other systems
such as \indexterm{Make} as a back-end.
The general workflow is:
\begin{enumerate}
\item The configuration stage. Here the \indextermtt{CMakeLists.txt} file
  is parsed, and \n{build} directory populated. This typically looks like:
\begin{verbatim}
mkdir build
cd build
cmake <source location>
\end{verbatim}
Some people create the build directory in the source tree,
in which case the cmake command is
\begin{verbatim}
cmake ..
\end{verbatim}
Other put the build directory next to the source, in which case:
\begin{verbatim}
cmake ../src_directory
\end{verbatim}
\item The build stage. Here the installation-specific compilation
  in the build directory is performed.
  With Make as the backend this would be
\begin{verbatim}
cd build
make
\end{verbatim}
but more generally
\begin{verbatim}
cmake --build <build directory>
\end{verbatim}
\item The install stage. This can move binary files to a permanent
  location, such as putting library files in \n{/url/lib}.
\end{enumerate}

\begin{verbatim}
[3] cmake --version
cmake version 3.19.2
\end{verbatim}

\Level 1 {Script structure}

CMake is driven by the \indextermttdef{CMakeLists.txt} file.
This needs to be in the root directory of your project.
(You can additionally have files by that name in subdirectories.)
See further section~\ref{sec:cmake-script}.

\Level 1 {Executable}

If you have a project that is supposed to deliver an executable,
you declare in your \texttt{CMakeLists.txt}:
\begin{lstlisting}
add_executable( myprogram programmain.c )
\end{lstlisting}

If there is only one source file, this is all you need.
However, often you will build libraries.
\begin{lstlisting}
add_library( mylibrary libsource.c libheader.h )
target_link_libraries( myprogram PRIVATE mylibrary )
\end{lstlisting}

By default the library is build as a static \texttt{.a} file,
but adding
\begin{lstlisting}
add_library( mylibrary SHARED libsource.c libheader.h )
\end{lstlisting}
or adding a runtime flag
\begin{verbatim}
cmake -D BUILD_SHARED_LIBS=TRUE
\end{verbatim}
changes that to a shared \texttt{.so} type.

\Level 1 {Directory structure}

If your sources are spread over multiple directories,
there needs to be a \texttt{CMakeLists.txt} file in each.
For instance, a library directory would have a file with only
\begin{lstlisting}
add_library( hello_lib SHARED
             hello.c hello.h )
\end{lstlisting}
to build the library file from the sources indicated.

The main file would then specify
\begin{lstlisting}
target_include_directories
   ( hellolibdir PUBLIC
     "${CMAKE_CURRENT_SOURCE_DIR}/hellolib" )
\end{lstlisting}
giving a path relative to the directory of the main file.

\Level 1 {Macro definitions}

Another thing that cmake can provide is defining macros.
\begin{lstlisting}
target_compile_definitions
  ( programname PUBLIC
    HAVE_HELLO_LIB=1 )
\end{lstlisting}

\Level 0 {CMake scripting}
\label{sec:cmake-script}

The \indextermtt{CMakeLists.txt} file is a script,
though it doesn't much look like it.
\begin{itemize}
\item
  Instructions consist of a command, followed by a parenthesized
  list of arguments.
\item
  (All arguments are strings: there are no numbers.)
\item
  Each command needs to start on a new line, but otherwise
  whitespace and line breaks are ignore.
\end{itemize}

Comments start with a hash character.

\Level 1 {Variables}

Variables are set with \texttt{set}.
Using the variable by itself gives the value,
except in strings, where a shell-like notation is needed:
\begin{lstlisting}
SET(SOME_ERROR "An error has occurred")
message(STATUS "${SOME_ERROR}")
set(MY_VARIABLE "This is a variable")
message(STATUS "Variable MY_VARIABLE has value ${MY_VARIABLE}")
\end{lstlisting}

Some variables are set by other commands.
For instance the \texttt{project} command sets
\indextermtt{PROJECT_NAME} and \indextermtt{PROJECT_VERSION}.


% LocalWords:  Eijkhout CMake workflow CMakeLists txt cmake mylibrary
% LocalWords:  programmain libsource libheader lib hellolibdir
